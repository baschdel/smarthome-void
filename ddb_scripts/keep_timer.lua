local function keep_timer__update_output(this)
	this.output = (not this.is_off) and (this.is_input_on or this.timer_on)
end

local function keep_timer_set_off_value(this, value)
	if (not value) == this.is_off then
		this.is_off = not not value --convert to bool
		if this.is_off then
			timer_on = false
		end
		this:_update_output()
	end
end

local function keep_timer_set_input_value(this, value)
	if (not value) == this.is_input_on then
		this.is_input_on = not not value --convert to bool
		if (not this.is_off) and (not this.is_input_on) then
			this.timer_on = true
			this.turned_off_time = os.time()
		end
		this:_update_output()
	end
end

-- returns true when an output changed
local function keep_timer_tick(this)
	if this.turned_off_time and this.timer_on then
		if os.time()-this.timeout > this.turned_off_time then
			this.timer_on = false
			this.turned_off_time = false
			this:_update_output()
			return true
		end
	end
	return false
end

local function keep_timer_reset(this)
	this.timer_on = false
	this.turned_off_time = false
	this:_update_output()
end

function KeepTimer(timeout)
	local this = {
		set_input_value = keep_timer_set_input_value,
		set_off_value = keep_timer_set_off_value,
		tick = keep_timer_tick,
		reset = keep_timer_reset,
		_update_output = keep_timer__update_output,
		timer_on = false,
		turned_off_time = false,
		timeout = timeout or 30,
		output = false,
		is_input_on = false,
		is_off = false,
	}
	return this
end

return {
	KeepTimer = KeepTimer,
}
