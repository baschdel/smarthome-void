io.read()
print("+ asw")
io.read()
print("> asw type register_file")
io.read()
print("+ outlets")
io.read()
print("> outlets type register_file")
io.read()

local commands = {}

local function add_gpio_pin(name, pin)
	commands["> "..name.." on"] = "raspi-gpio set "..pin.." op pn dh > /dev/null"
	commands["> "..name.." off"] = "raspi-gpio set "..pin.." op pn dl > /dev/null"
end

add_gpio_pin("asw x", 21)
add_gpio_pin("asw y", 20)
add_gpio_pin("asw z", 16)

add_gpio_pin("asw l", 12)

add_gpio_pin("outlets 1", 7)
add_gpio_pin("outlets 2", 8)
add_gpio_pin("outlets 3", 25)
add_gpio_pin("outlets 4", 24)
add_gpio_pin("outlets 5", 23)
add_gpio_pin("outlets 6", 18)
add_gpio_pin("outlets 7", 15)
add_gpio_pin("outlets 8", 14)

while true do
	local i = io.read()
	if not i then break end
	local command = commands[i]
	if command then
		os.execute(command)
	end
end
