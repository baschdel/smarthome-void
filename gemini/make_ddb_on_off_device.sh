# Usage: <devdir> <name> <class>

mkdir -p "$1"
cd "$1"
echo "$3" > class
NAME="$2"

script_base () {
	echo "#!/bin/bash"
	echo ". ~/.profile"
	echo "NAME='$NAME'"
	echo "echo 20 text/plain"
}

on_script () {
	script_base
	echo "printf '> %s on\n' '$NAME' | ddb cat"
	echo 'echo on'
}

off_script () {
	script_base
	echo "printf '> %s off\n' '$NAME' | ddb cat"
	echo 'echo off'
}

status_script () {
	script_base
	echo "ddb read_value $NAME"
}

on_script > on
off_script > off
status_script > state

chmod +x on
chmod +x off
chmod +x state

