# Usage: <devdir> <name> <class> <states>

mkdir -p "$1"
cd "$1"
echo "$3" > class
NAME="$2"

script_base () {
	echo "#!/bin/bash"
	echo ". ~/.profile"
	echo "NAME='$NAME'"
	echo "echo 20 text/plain"
}

set_state_script () {
	script_base
	echo "printf '> %s %s\n' '$NAME' '$1' | ddb cat"
	echo 'printf ok'
}

status_script () {
	script_base
	echo "ddb read_value $NAME"
}

for name in `printf "%s" "$4" | tr ' ' '\n'`; do
	set_state_script "$name" > $name
	chmod +x "$name"
done

status_script > state
chmod +x state

