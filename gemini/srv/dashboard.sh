#!/bin/bash
echo "20 text/gemini"

export DDB_PORT='60707'
export DDB_GREETING='<put your DDB_GREETING here>'

get_state () {
	~/.local/bin/ddb_connect "/home/pi/.local/bin/ddb_read_value $1 $2" 2>&1
}

get_amp_mode_text () {
	get_state requests amp_mode | sed 's/^J$/Primary/' | sed 's/^T$/Secondary/' | sed 's/^JT$/Both/' | sed 's/^JS$/Jumpstart/'
}

echo "# Dashboard"

echo "## Audio Routing"
echo "### Audio amber ($(get_state requests audio-amber))"
echo "=> /obj/audio_amber/off OFF"
echo "=> /obj/audio_amber/on ON"
echo ""
echo "### Audio tv ($(get_state requests audio-tv))"
echo "=> /obj/audio_tv/off OFF"
echo "=> /obj/audio_tv/on ON"
echo ""
echo "### Audio aux ($(get_state requests audio-aux))"
echo "=> /obj/audio_aux/off OFF"
echo "=> /obj/audio_aux/on ON"
echo ""
echo "### Amplifier Mode ($(get_amp_mode_text))"
echo "=> /obj/amp_mode/J Primary"
echo "=> /obj/amp_mode/T Secondary"
echo "=> /obj/amp_mode/JS Jumpstart"
echo "=> /obj/amp_mode/JT Both"
echo ""

echo "## Services"
echo '``` lord status'
~/.local/bin/lord status
echo '```'
